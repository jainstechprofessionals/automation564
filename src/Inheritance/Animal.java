package Inheritance;

public class Animal {
	
	
	public Animal() {
		System.out.println("in animal constructor");
	}
	
	public Animal(String i) {
		System.out.println(i);
	}
	
	
	
	public void eat() {
		System.out.println("This Animal eat food");
	}

	
	public void print() {
		System.out.println("in animal");
	}
	
	
}
