package Inheritance;

public class Dog extends Animal {

	
	public Dog() {
		super();
		System.out.println("in dog constructor");
	}
	
	public void bark() {
		
		super.eat();

		System.out.println(" dog barks");
	}
	
	public void print() {
		System.out.println("in dog");
	}

}
