package Inheritance;

import java.util.Scanner;

public class MainClass {
	Bank obj;
	
	
	public int getR(String str) {
		Scanner s = new Scanner(System.in);
		
		int r = 0; 
		if(str.equalsIgnoreCase("icici")) {
			obj = new ICICI(); 
			r= obj.getRateOfIntrest();
		}
		
		if(str.equalsIgnoreCase("SBI")) {
			obj = new SBI(); 
			r= obj.getRateOfIntrest();
		}
		
		 return r;
	}
	

	public static void main(String[] ars) {

//		ICICI ic = new ICICI();   //static binding ,  early binding
//		int i = ic.getRateOfIntrest();
//		System.out.println(i);
//		
//		System.out.println(ic.a);
//		
		Bank obj;
//		
		obj = new Bank();
		System.out.println(obj.getRateOfIntrest());
		
		
		
		obj = new ICICI(); // run time polymorphism , dynamic method dispatch , upcasting, late binding , dynamic binding 
		System.out.println(obj.getRateOfIntrest());
		
		
//		BabyDog obj = new BabyDog();
//		
//		obj.weep();

//		ChildClass obj = new ChildClass();
		
		
		
	}

	// Parent class

}
