package Inheritance;

public class BabyDog extends Dog{

	public BabyDog() {
		
		System.out.println("in babydog constructor");
	}
	
	
	public void weep(){
		super.print();
		this.print();
		System.out.println("baby dog weep");
	}
	
	public void print() {
		System.out.println("in babydog");
	}
}
