package CollectionsFramework;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetClass {

	public static void main(String[] args) {
		
		
		HashSet<String> hs = new HashSet<String>();
		hs.add("Raunak");
		hs.add("Raunak");
		hs.add("keenal");
		hs.add("khusboo");
		hs.add("jain");
		hs.add(null);
		
		Iterator<String> i = hs.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
		}

	}

}
