package CollectionsFramework;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListClass {

	public static void main(String[] args) {

		LinkedList <String> arStr = new LinkedList<String>();
		
		arStr.add("raunak");
		arStr.add("keenal");
		arStr.add("Tarun");			
		arStr.add(2, "khusboo");		
		arStr.add("raunak");	
		arStr.remove(2);
			
		
//		for(int i=0 ;i<arStr.size();i++) {
//			System.out.println(arStr.get(i));
//		}
		
		Iterator itr = arStr.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
//		for(String str : arStr) {
//			System.out.println(str);
//		}
		

	}

}
