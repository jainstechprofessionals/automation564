package CollectionsFramework;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class TreeSetClass {

	public static void main(String[] args) {
		
		
		TreeSet<String> hs = new TreeSet<String>();
		hs.add("Raunak");
		hs.add("Raunak");
		hs.add("keenal");
		hs.add("khusboo");
		hs.add("jain");
		
		
		Iterator<String> i = hs.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
		}
	}

}
