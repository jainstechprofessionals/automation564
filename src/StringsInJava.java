
public class StringsInJava {

	public static void main(String[] args) {

		// mutable
		// immutable

		// 1. String literals

		String str = "rounak";

		String str1 = "rounak";

		
		
		if (str == str1) {
			System.out.println("equlas");
		} else {
			System.out.println("not equal");
		}

		// 2. Using new keyword

		String str2 = new String("   rounak     ");
		String str3 = new String("rounak");
			
		str2 = str2.trim();
		
	
		
		if (str2.equalsIgnoreCase(str3)) {
			System.out.println("equlas");
		} else {
			System.out.println("not equal");
		}
		
		
		
		String str5 =  "Rounak Jain Class";
		
		String [] str6 = str5.split(" ");
		
		System.out.println(str6[2]);
		
		System.out.println("****************************");
		
		char [] ch = str.toCharArray();
		
		System.out.println(ch.length);
		
		System.out.println(ch[3]);
		
		
		char ch1[] = {'k','e','e','n','a','l'};
		
		String s = new String(ch1);
		System.out.println(s);
		
		System.out.println(str.toUpperCase());
		

	}
	
	
	
	
	
	// variable
	// data type
	// operators
	// conditional statements
	// loops
	// methods
	// constructor
	// object and class
	// Static 
	// STring
	// Array
	// method overloading
	// this
	//final
	// overriding
	
	

}
