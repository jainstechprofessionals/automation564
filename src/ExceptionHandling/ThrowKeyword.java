package ExceptionHandling;

public class ThrowKeyword {

	public static void main(String[] args) throws UserException {

		int age = 17;
//		try {
//			if (age < 18) {
////				System.out.println("not valid");
//				throw new UserException(17);
//			} else {
//				System.out.println("valid");
//			}
//		} catch (UserException e) {
//			System.out.println(e);
//		}
		
		
		
		if (age < 18) {
//			System.out.println("not valid");
			throw new UserException(17);
		} else {
			System.out.println("valid");
		}
		
		
		
		

	}

}
