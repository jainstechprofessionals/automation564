
public class OperatorsInJava {

	public static void main(String[] args) {
		
		
		OperatorsInJava oj =new OperatorsInJava();
		
		
	
		// Aritmatic  Operators 
		  // * , / ,% , +,-
		
		int a= 2, b=5;
		
//		System.out.println(a+b);  // 
//		System.out.println(a-b);  // 
//		System.out.println(a*b);  // 
//		
//		
//		System.out.println(a/b);  // 4
//		System.out.println(a%b); //  1
	
		
		// Assignment Operators 
		
		a=b;
		
//		a += 2;  // a= a+2;
//		System.out.println(a);
		
		// Realtional operator
		// > , < , >= , <= , == , !=
		
//		System.out.println(a==b);
		
		
		// unary Operators 
		 // exp ++ , exp --, --exp , ++exp
		
		int i=2;
//		 i++; // i=i+1;
//		++i;
		
		
//		System.out.println(i);
		
		int temp ;
		
//		temp=  i+1;
		
//		temp =  i  + i++;     // (2 + 2)
//		System.out.println(i);    // 3
//		System.out.println(temp); // 4
				
		
		
//		temp = i++ + i;  // (2 + 3 )
//		
//		System.out.println(i);    // 2,3,2,2
//		System.out.println(temp); // 5,5,5,5
		
		
//		temp = i + i + i++ + i++ - i;  // ( 2 + 2 + 2 + 3 -4)	
//		System.out.println(i);     //  4,
//		System.out.println(temp);   // 5
		
		
		
//		temp = i + i++ + ++i;  // ( 2+2+4) (2+2+4) (2+2+4) (2+2+4)
//		System.out.println(i);     //  4, 4, 4,4
//		System.out.println(temp);   // 8,8,8,8
		
		
//		temp = i + i - i + ++i - --i + i++ + ++i + --i; // (2+2-2+3-2+2+4+3) (2+2-2+3-2+2+3+2) 		
//		// (2+2-2+3-2+2+4+2) (2+2-2+3-2+2+3+2)				
//		System.out.println(i);  //3 ,  2 , 2, 2
//		System.out.println(temp); //12, 
		
		i=2;
//		temp = --i + i++;   // (1+1)
//		System.out.println(i);  //2
//		System.out.println(temp); //2 
		
		temp = --i + i-- + i++ + ++i;    // (1 + 1 + 0 +  2  )
		System.out.println(i);  //2
		System.out.println(temp);
		
		
		
	}

}
