
public class MethodsInJava {

	
	// Methods (Functions)
	// Parameterized 
	// Non Parameterized 	
	
	
	public void show() {
		System.out.println("hello");
	}
	
	public void print(String name) {
		System.out.println(name );
	}
	
	// private,  String , display, @ parameters (int , String) 
	
	private String display(int a, String s) {
		System.out.println();
		return "sadfa";
	}
	
	// protected , float , view, parameter 
	
	protected float  view(int a , String b, short c) {
		return 3.5f;
	}	
	// default, short , see ( 1 parameter)
		
	 short see(int i) {
		return 12;
	}
		
	 // public void show . no parameter
	 
	 public void show1(int a) {
		 
	 }	 
	// private void abc (1 parameter)
	 private void abc (String str) {
		 
	 }	 
	 // public , double , area, (2 parameter (int and float))
	 
	 public double area (int a , float f) {
		 return 45;
		 
	 }
	 
	 // default, void nothing (no parameter)
	 
	 void nothing () {
		 
	 }
	 	
	
	public static void main(String[] args) {
		
		MethodsInJava abc = new MethodsInJava(); //Instantiate 
		
		abc.print("rounak");		// call , invoke

		
		
	}

}
