
public class OverLoadingProgram {
	int ar, a, b, r;

	OverLoadingProgram() {
		this(10);
		System.out.println("in default const");
	}

	OverLoadingProgram(int a) {

		System.out.println("in parametrized const");
	}

	/**
	 * This method is calculating the area of circle
	 * 
	 * @param r
	 */
	public void area(int r) {
		this.r = r;
		System.out.println(Math.PI * r * r);
		int ar = 10;
		this.ar = ar;

	}

	/**
	 * area of rectangle
	 * 
	 * @param a
	 * @param b
	 */

	public void area(int a, int b) {
		this.area(10.0f);
		System.out.println(a * b);
	}

	/**
	 * square
	 * 
	 * @param a
	 */
	public void area(float a) {
		area(10, 20);
		System.out.println(a * a);
	}

	public void swap(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public void printSwappedValue() {
		System.out.println(a);
		System.out.println(b);
	}

	public void entry(int i) {

	}

	public void entry(String i) {

	}

	public void entry(float i) {

	}

	public static void main(String[] args) {
		OverLoadingProgram obj = new OverLoadingProgram();
		obj.swap(10, 20);
		obj.printSwappedValue();

	}

}
