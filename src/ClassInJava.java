
public class ClassInJava {
	
	int a =20;   //data members
	
	
	float b ;
	
	
	public void singer() {       // member functions 
		
	}
	
	
	public String  evenOrOdd(int b) {  // non parameterized method
		
		if(b%2==0) {
//			System.out.println("number is even");
//			return true;
			return "number is even";
		}else  {
//			System.out.println("number is odd");
//			return false;
			return "number is odd";
		}
	}
	  
//	0 1 1 2 3 5 8 13
	
	public void fibonacci() {
		
		int x =0, y=1,z;
		System.out.println(x);
		System.out.println(y);
		
		for(int i=2;i<19;i++) {
			
			z=x+y;
			System.out.println(z);
			
			x=y;
			y=z;
			
		}
		
	}
	
	
	
	public float  eOrOdd (String i) {  //parameterized method
		
		return 2.3f;
	}
	
	

	public static void main(String[] args) {
		
		ClassInJava obj = new ClassInJava();
//		String bbb =	obj.evenOrOdd(20);
//		
//		System.out.println(bbb);
//		
//		 bbb =	obj.evenOrOdd(25);
//		 System.out.println(bbb);
		obj.fibonacci();
	}	

	

}
