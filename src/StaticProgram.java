
public class StaticProgram {
	
	int rollNo;
	String name;
	 static String college = "IPS";
	
	 
	 static {
		 System.out.println("in static block");
	 }
	 
	public void get(int r, String n) {
		rollNo = r;
		name = n;
	}
	
	public void display() {
	
		System.out.println("roll no is "+ rollNo);
		System.out.println("name  is "+ name);
		System.out.println("college is "+ college);
	}
	
	public void d() {
		get(10,"sadf");
	}
	
	
	public static void asdfsa() {
		
	}
	
	public static void main(String[] args) {
		StaticProgram ob = new StaticProgram();
		ob.get(1, "khusboo");
		ob.display();
		
		StaticProgram.college = "LNCT";
		
		StaticProgram ob1 = new StaticProgram();
		ob1.get(2, "Keenal");
		ob1.display();

		
		
		StaticProgram.asdfsa();
		
	}

	
	// fibonacci
	// palindrome
	// factorial
	
	

}
